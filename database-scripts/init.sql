DROP TABLE IF EXISTS person;

CREATE TABLE person(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255),
    first_name VARCHAR(255)
);

INSERT INTO person (name,first_name) VALUES ('Test','Toust');